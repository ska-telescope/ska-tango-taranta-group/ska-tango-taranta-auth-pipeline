image: $SKA_K8S_TOOLS_BUILD_DEPLOY

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CHARTS_TO_PUBLISH: ska-tango-taranta-auth
  JS_PROJECT_DIR: ./taranta-auth
  JS_NODE_VERSION: 18
  UPSTREAM_REPOSITORY: https://gitlab.com/tango-controls/web/taranta-auth.git
  MINIKUBE: "false"

cache:
  paths:
    - ${JS_PROJECT_DIR}
    - build

stages:
  - deploy-stfc
  - clone
  - lint
  - build
  - test
  - scan
  - pages
  - publish
  - production

clone-repo:
  stage: clone
  tags:
    - k8srunner
  script:
    - rm -fR ${JS_PROJECT_DIR}/
    - git clone ${UPSTREAM_REPOSITORY}
    - cd ${JS_PROJECT_DIR}/
    - cat $TRIGGER_PAYLOAD > payload.json
    - if [ -z $BRANCH ]; then git checkout $(jq -r '.checkout_sha | select( . != null )' payload.json); else git checkout $BRANCH; fi
  artifacts:
    paths:
      - ${JS_PROJECT_DIR}/

# Needed because the existing rules don't apply here because
# taranta-auth repo is cloned afterwards
js-lint:
  rules:
    - when: always
  allow_failure: true

# Needed because the existing rules don't apply here because
# taranta-auth repo is cloned afterwards
js-test:
  variables:
    JS_JEST_SWITCHES: "--runInBand"
  rules:
    - when: always

install-taranta-auth-chart:
  stage: deploy-stfc
  tags:
    - k8srunner
  when: manual
  variables:
    KUBE_NAMESPACE: "taranta-auth"
    HELM_RELEASE: "taranta-auth"
    HELM_RELEASE_PVC: "taranta-auth-pvc"
  script:
    - make k8s-install-chart
    - make k8s-wait
  environment:
    name: taranta-auth
    on_stop: uninstall-taranta-auth-chart
    url: "https://k8s.stfc.skao.int/taranta-namespace/auth"
    kubernetes:
      namespace: $KUBE_NAMESPACE

uninstall-taranta-auth-chart:
  stage: deploy-stfc
  tags:
    - k8srunner
  when: manual
  variables:
    KUBE_NAMESPACE: "taranta-namespace"
  script:
    - make k8s-uninstall-chart
  environment:
    name: "taranta-auth"
    action: stop
    url: "https://k8s.stfc.skao.int/taranta-namespace/auth"
    kubernetes:
      namespace: $KUBE_NAMESPACE

deploy-auth:
  stage: production
  tags:
    - ska-aws-taranta
  variables:
    KUBE_NAMESPACE: "taranta"
    K8S_SKIP_NAMESPACE: true
    HELM_RELEASE: "auth"
    K8S_CHART: "ska-tango-taranta-auth"
    K8S_CHART_VERSION: "${CI_COMMIT_TAG}" 
    K8S_CHART_PARAMS: "-f charts/ska-tango-taranta-auth/environments/production.values.yml"
  script:
    - make k8s-install-chart-car
    - make k8s-wait
  when: manual
  environment:
    name: "taranta-auth"
    kubernetes:
      namespace: $KUBE_NAMESPACE
  rules: 
    - if: $CI_COMMIT_TAG

# Standardised included jobs

include:

  # Javascript
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/js.gitlab-ci.yml'

  # OCI Images
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/oci-image.gitlab-ci.yml'

  # Helm Charts
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/helm-chart.gitlab-ci.yml"

  # changelog release page
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/changelog.gitlab-ci.yml"

  # .post step finalisers eg: badges
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/finaliser.gitlab-ci.yml"
