KUBE_NAMESPACE?=testnamespace
CI_COMMIT_SHA?=local
MINIKUBE?=true
HELM_RELEASE?=taranta-auth
HELM_CHARTS?=ska-tango-taranta-auth
PROJECT=ska-tango-taranta-auth

JS_PROJECT_DIR ?= ./taranta-auth
JS_PACKAGE_MANAGER ?= npm
JS_ESLINT_CONFIG ?= .eslintrc.json
JS_JEST_SWITCHES ?=

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker
STAGE ?= build_react_artefacts

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include core make support
include .make/base.mk

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include js support
include .make/js.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

oci-pre-build:
	@echo $(USERS) > taranta-auth/users.json