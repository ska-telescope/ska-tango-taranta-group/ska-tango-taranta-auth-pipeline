FROM node:8-alpine
WORKDIR /var/app

COPY taranta-auth/package.json taranta-auth/package-lock.json ./
RUN npm install --no-cache

COPY taranta-auth/src src
COPY taranta-auth/users.json /var/users.json

CMD ["node", "src"]
